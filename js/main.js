(()=>{
    "use strict";

    console.log('linked');
    const div = document.querySelector('#div');
    div.style.backgroundColor = 'teal';
    div.style.width = '300px';
    div.style.height = '100px';
    div.innerHTML = `<p id="p">I come from JS</p>`;
    document.querySelector('#p').style.color = 'white';
})();